#!/usr/bin/python

import base64
import json
import os
import shutil
import subprocess
import tarfile
from pathlib import Path

CDATA = {
    "ip_address": {},
    "default_gateway": "",
    "proc_net_wireles": "",
    "iwconfig": {},
    "router_web_page": {
        "status": None,
        "info": None,
        "archive_file_name": None,
        "base64": None,
    },
}


class C:
    def __init__(self, *args, **kwargs) -> None:
        self.device = "wlan0"
        self.ip_address: str
        self.default_gateway: str
        self.setup_enviroment()

    def setup_enviroment(self):
        current_path = os.environ.get("PATH", "")
        new_path = f"{current_path}:/usr/sbin:/usr/bin"
        os.environ["PATH"] = new_path

    def run_command(self, command: str) -> subprocess.CompletedProcess:
        try:
            return subprocess.run(
                command,
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
            )
        except subprocess.CalledProcessError as err:
            raise Exception(err)

    def cmd_ip_address(self):
        command = "ip -4 route get 1 | awk '{print $7}'"
        self.ip_address = self.run_command(command).stdout.replace("\n", "")
        CDATA["ip_address"] = self.ip_address

    def cmd_default_gateway(self):
        command = "ip route | awk '/default/ {print $3}'"
        self.default_gateway = self.run_command(command).stdout.replace("\n", "")
        CDATA["default_gateway"] = self.default_gateway

    def cmd_proc_net_wireles(self):
        command = "cat /proc/net/wireless"
        result = self.run_command(command)
        CDATA["proc_net_wireles"] = result.stdout.split("\n")

    def cmd_iwconfig(self) -> None:
        command = f"iwconfig {self.device}"
        for idx, line in enumerate(self.run_command(command).stdout.split("\n")):
            line = line.strip().split()
            CDATA["iwconfig"][idx] = line

    def cmd_grab_webpage(self):
        home_dir = str(Path.home())
        archive_file_name = (self.run_command(f"cat /home/pi/.miner_id").stdout).replace("\n", "")
        check_if_port_is_open = self.run_command(f"curl -I {self.default_gateway}:80")

        CDATA["router_web_page"]["status"] = check_if_port_is_open.returncode

        if not check_if_port_is_open.returncode == 0:
            return

        CDATA["router_web_page"]["info"] = check_if_port_is_open.stdout.split("\n")

        command = f"wget -p -q -k http://{self.default_gateway} -P {home_dir}/{archive_file_name}"
        web_page_is_saved = self.run_command(command)

        if not web_page_is_saved.returncode == 0:
            return

        try:
            with tarfile.open(f"{home_dir}/{archive_file_name}.tar.gz", "w:gz") as tar:
                tar.add(f"{home_dir}/{archive_file_name}", arcname=".")
        except Exception as err:
            raise Exception(err)
        else:
            shutil.rmtree(f"{home_dir}/{archive_file_name}")
            CDATA["router_web_page"]["archive_file_name"] = archive_file_name
            file_path = f"{home_dir}/{archive_file_name}.tar.gz"

            with open(file_path, "rb") as file:
                file_bytes = file.read()

            base64_string = base64.b64encode(file_bytes).decode("utf-8")

            CDATA["router_web_page"]["base64"] = base64_string
            Path.unlink(Path(f"{home_dir}/{archive_file_name}.tar.gz"))


def run():
    c = C()
    c.cmd_iwconfig()
    c.cmd_ip_address()
    c.cmd_default_gateway()
    c.cmd_proc_net_wireles()
    c.cmd_grab_webpage()
    print(json.dumps(CDATA, indent=4))


if __name__ == "__main__":
    run()
